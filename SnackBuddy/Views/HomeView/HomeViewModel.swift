//
//  HomeViewModel.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 30/03/2022.
//

import SwiftUI
import os.log

class HomeViewModel: ObservableObject {
    private var userRepository = UserRepository()
    private var achievementRepository = AchievementRepository()
    private var snackRepository = SnackRepository()
    
    private var colorIndex: Int = 0
    private let colors: [Color] = [Color("Red"), Color("Yellow"), Color("Green")]
    private let gridLayout = [GridItem(.fixed(120)), GridItem(.fixed(120))]
    private let currentHour = Calendar.current.component(.hour, from: Date())
    
    @Published var uncompletedAchievements: [Achievement]?
    @Published var completedAchievements: [Achievement]?
    
    @Published var snacks: [Snack]?
    @Published var randomSnack: Snack?
    @Published var choosenSnack: Snack?
    
    @Published var errorAlert: ErrorAlert?
    @Published var currentAchievementIndex = 0
    
    /**
     Retrieves a random snack from the database store.
     
     - Parameter errorHandler: The `ErrorHandler` object that is used to show an error message when something goes wrong.
     
     - Throws: `DatabaseError.retrievingError`
     If an error occurs when retrieving the random snack from the database store.
     */
    func retrieveRandomSnack(errorHandler: ErrorHandler) {
        // Check if all snacks have been retrieved and retrieve them if not.
        if snacks == nil {
            snackRepository.getAll { snacks in
                if snacks != nil {
                    self.snacks = snacks
                    self.randomSnack = snacks!.randomElement()
                } else {
                    errorHandler.handle(error: DatabaseError.retrievingError)
                }
            }
        } else {
            randomSnack = snacks!.randomElement()
        }
    }
    
    /**
     Sets the current random snack as the chosen snack for this time of day.
     */
    func chooseSnack() {
        choosenSnack = randomSnack
    }
    
    /**
     Retrieves the achievements from the `achievementsRepository`.
     
     - Parameter errorHandler: The `ErrorHandler` object that is used to show an error message when something goes wrong.
     
     - Throws: `DatabaseError.retrievingError`
     If an error occurs when retrieving the achievements from the database store.
     */
    func retrieveAchievements(errorHandler: ErrorHandler) {
        let userId = UserDefaults.standard.string(forKey: "user_id")
        
        // Checks if the user ID is nil and shows an error message.
        if userId == nil {
            Logger.user.error("Error retrieving the user ID, it is nil")
            errorHandler.handle(error: UserError.retrievingError)
            
            completedAchievements = []
            uncompletedAchievements = []
            
            return
        }
        
        // Get the user object.
        userRepository.getById(userId!) { user in
            self.achievementRepository.getAll(user.completedAchievements) { achievements in
                if let achievements = achievements {
                    self.completedAchievements = achievements[0]
                    self.uncompletedAchievements = achievements[1]
                } else {
                    errorHandler.handle(error: DatabaseError.retrievingError)
                }
            }
        }
    }
    
    /**
     Gets the part of the day as a string.
     
     - Returns: A string that holds the day part name.
     */
    func getDayPart() -> String {
        if currentHour >= 0 && currentHour < 12 {
            return "deze ochtend"
        } else if currentHour >= 12 && currentHour < 18 {
            return "vanmiddag"
        }
        
        return "vanavond"
    }
    
    /**
     Gets the greeter text that needs to be displayed on the `HomeView`
     
     - Returns: A `String` with the greeter text.
     */
    func getGreeterText() -> String {
        if currentHour >= 0 && currentHour < 12 {
            return "Goedemorgen"
        } else if currentHour >= 12 && currentHour < 18 {
            return "Goedemiddag"
        }
        
        return "Goedenavond"
    }
    
    /**
     Determines the color that the background of an `AchievementButton` should have.
     
     - Returns: The `Color` that an `AchievementButton` should have.
     */
    func setAchievementColor() -> Color {
        let color = colors[colorIndex]
        
        if colorIndex == colors.count - 1 {
            colorIndex = 0
        } else {
            colorIndex += 1
        }
        
        return color
    }
    
    /**
     Builds the correct view for the achievements row on the `HomeView`.
     
     - Parameter achievements: An array of the achievements that need to be displayed in a `LazyHGrid`.
     
     - Returns:
     A `ProgressView` when the given array is nil, a `Text` view letting the user know there aren't any achievements or a `LazyHGrid` with the achievements from the given array.
     */
    func buildAchievementRow(achievements: [Achievement]?) -> some View {
        if achievements == nil {
            return AnyView(ProgressView().padding())
        } else if achievements!.count == 0 {
            return AnyView(
                Text("Er zijn nog geen prestaties.")
                    .font(.custom("Merriweather", size: 16))
                    .foregroundColor(.gray)
                    .padding([.bottom, .leading, .trailing])
            )
        }
        
        return AnyView(
            LazyHGrid(rows: gridLayout) {
                ForEach(achievements!) { achievement in
                    AchievementButton(achievement: achievement, color: self.setAchievementColor())
                }
            }.padding(.horizontal)
        )
    }
    
    /**
     Builds the snack view that is used to display the image and name of the random snack.
     */
    func buildSnackImageView() -> some View {
        let imageView = AsyncImage(url: URL(string: choosenSnack != nil ? choosenSnack!.image : randomSnack!.image)) { image in
            image
                .resizable()
                .aspectRatio(contentMode: .fit)
        } placeholder: {
            ProgressView()
        }.padding(.horizontal)
        
        // The view that needs to be shown if the user hasn't choosen a snack yet.
        let randomSnackImageView = HStack {
            imageView
            
            Spacer()
        }
        
        // The view that needs to be shown if the user has already choosen a snack.
        let choosenSnackImageView = HStack {
            ZStack {
                imageView.opacity(0.5)
                
                Image(systemName: "checkmark.circle.fill")
                    .foregroundColor(Color("Green"))
                    .font(.system(size: 60.0))
            }
            
            Spacer()
        }
        
        if choosenSnack == nil {
            return AnyView(randomSnackImageView)
        }
        
        return AnyView(choosenSnackImageView)
    }
}
