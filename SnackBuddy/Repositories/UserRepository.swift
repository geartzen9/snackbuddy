//
//  UserRepository.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 01/04/2022.
//

import FirebaseFirestore
import os.log

class UserRepository: Repository {
    typealias T = User
    
    var path: String = "users"
    var store = Firestore.firestore()
    
    /**
     Sets the settings of the firestore instance.
     */
    init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        
        store.settings = settings
    }
    
    /**
     Adds a new document to the database store.
     
     - Parameter model: The model instance that needs to be added as a document to the database store.
     
     - Throws: `Never`
     if an error occurs when adding a new document to the database store.
     */
    func add(completion: @escaping(String?) -> Void) {
        var documentReference: DocumentReference? = nil
        
        documentReference = store.collection(path).addDocument(data: [
            "coins": 0,
            "completedAchievements": []
        ]) { error in
            if let error = error {
                Logger.documents.error("Error adding user: \(error.localizedDescription)")
                completion(nil)
            } else {
                completion(documentReference!.documentID)
            }
        }
    }
    
    /**
     Gets a single document that corresponds to the given ID from the database store.
     
     - Parameter id: The ID of the document that needs to be retrieved.
     
     - Throws: `Never`
     if an error occurs when fetching the document from the database store.
     
     - returns: The retrieved document.
     */
    func getById(_ id: String, completion: @escaping(User) -> Void) {
        store.collection(path)
            .document(id)
            .getDocument { (document, error) in
                guard let document = document else { return }
                guard let user = try? document.data(as: User.self) else { return }
                
                completion(user)
            }
    }
}
