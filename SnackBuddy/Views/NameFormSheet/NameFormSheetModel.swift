//
//  NameFormSheetModel.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 22/04/2022.
//

import Foundation
import SwiftUI

class NameFormSheetModel: ObservableObject {
    private var userRepository = UserRepository()

    @Published var firstname: String = ""

    func createUser(errorHandler: ErrorHandler) {
        userRepository.add { userId in
            if let userId = userId {
//                print("Bier")
                errorHandler.handle(error: DatabaseError.addingError)
            } else {
//                print("Pils")
                UserDefaults.standard.set(self.firstname, forKey: "firstname")
//                self.showing = false
            }
        }
    }
}
