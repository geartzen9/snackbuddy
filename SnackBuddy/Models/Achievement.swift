//
//  Achievement.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/03/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Achievement: Identifiable, Decodable {
    @DocumentID var id: String?
    let icon: String
    let name: String
    let reward: Int
}
