//
//  UserError.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 27/06/2022.
//

import Foundation

/**
 An enum for the error messages that need to be shown when an error occurs with the current user..
 */
enum UserError: LocalizedError {
    case retrievingError
    case addingError
    
    var errorDescription: String? {
        switch self {
        case .retrievingError:
            return "Er is een fout opgetreden tijdens het ophalen van een aantal gegevens. Probeer het later opnieuw."
        case .addingError:
            return "Er is een fout opgetreden tijdens het opslaan van de gegevens. Probeer het later opnieuw."
        }
    }
}
