//
//  NumberContainer.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 04/06/2022.
//

import SwiftUI

struct CoinsContainer: View {
    private let viewModel = CoinsContainerViewModel()
    
    let amount: Int?
    let backgroundColor: Color
    let foregroundColor: Color
    let headingText: String?
    
    /**
     Constructor for initializing a new `CoinsContainer`.
     
     - Parameters:
        - amount: The amount of coins that need to be displayed inside the container.
        - backgroundColor: The background color of the container.
        - foregroundColor: The foreground color of the text in the container.
        - headingText: An optional title that is displayed above the `amount` of coins.
     */
    init(amount: Int?, backgroundColor: Color, foregroundColor: Color, headingText: String? = nil) {
        self.amount = amount
        self.backgroundColor = backgroundColor
        self.foregroundColor = foregroundColor
        self.headingText = headingText
    }
    
    var body: some View {
        if headingText != nil {
            return AnyView(
                VStack(alignment: .leading) {
                    headingText != nil ? AnyView(
                        Text(headingText!)
                            .font(.custom("Merriweather", size: 18).weight(.bold))
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.bottom, 1)
                    ) : AnyView(EmptyView())
                    
                    viewModel.buildTextView(amount: amount, foregroundColor: foregroundColor)
                }
                .padding()
                .frame(maxWidth: .infinity)
                .background(backgroundColor)
                .cornerRadius(15)
            )
        }
        
        return AnyView(
            viewModel.buildTextView(
                amount: amount,
                foregroundColor: foregroundColor,
                backgroundColor: backgroundColor
            )
        )
    }
}

struct CoinsContainer_Previews: PreviewProvider {
    static var previews: some View {
        CoinsContainer(
            amount: 200,
            backgroundColor: Color("Green"),
            foregroundColor: Color("Yellow"),
            headingText: "Huidig saldo:"
        )
    }
}
