//
//  BuddyShopItem.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 20/06/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct BuddyShopItem: Model {
    @DocumentID var id: String?
    let image: String
    let title: String
    let description: String
    let cost: Int
}
