//
//  SnackButton.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 03/06/2022.
//

import SwiftUI

struct SnackButton: View {
    let text: String
    let backgroundColor: Color
    let foregroundColor: Color
    let action: () -> Void
    let icon: String?
    
    init(text: String, backgroundColor: Color, foregroundColor: Color, action: @escaping () -> Void, icon: String? = nil) {
        self.text = text
        self.backgroundColor = backgroundColor
        self.foregroundColor = foregroundColor
        self.action = action
        self.icon = icon
    }
    
    var body: some View {
        Button(action: action) {
            HStack {
                icon != nil ? AnyView(Image(systemName: icon!)
                    .font(.system(size: 18.0))) : AnyView(EmptyView())
                
                Text(text)
                    .font(.custom("Merriweather", size: 18))
            }
            .padding([.top, .bottom], 12)
            .frame(maxWidth: .infinity)
            .background(backgroundColor)
            .foregroundColor(foregroundColor)
            .cornerRadius(15)
        }
    }
}

struct SnackButton_Previews: PreviewProvider {
    static var previews: some View {
        SnackButton(
            text: "Oké",
            backgroundColor: Color("Green"),
            foregroundColor: .white,
            action: {},
            icon: "checkmark.circle.fill"
        )
    }
}

