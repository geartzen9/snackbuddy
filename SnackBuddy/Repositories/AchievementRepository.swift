//
//  AchievementRepository.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 02/04/2022.
//

import FirebaseFirestore
import os.log

class AchievementRepository: Repository {
    typealias T = [Achievement]
    
    var path: String = "achievements"
    var store = Firestore.firestore()
    
    /**
     Sets the settings of the firestore instance.
     */
    init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        
        store.settings = settings
    }
    
    /**
     Fetches all completed and uncompleted achievements from the database store.
     
     - Parameters:
        - completedIds: The range of ID's of the completed achievement documents that needs to be retrieved.
        - completion: The closure that is executed when the documents have been fetched.
     
     - Returns: An array of two array's. The first index is an array containing the completed achievements and the second index the uncompleted axchievements.
     */
    func getAll(_ completedIds: [String], completion: @escaping ([[Achievement]]?) -> Void) {
        store.collection(path)
            .getDocuments { (snapshot, error) in
                if let error = error {
                    Logger.documents.error("Error retrieving achievements: \(error.localizedDescription)")
                    completion(nil)
                    return
                }
                
                var completedAchievements: [Achievement] = []
                var uncompletedAchievements: [Achievement] = []
                
                for document in snapshot!.documents {
                    guard let achievement = try? document.data(as: Achievement.self) else { return }
                    
                    if completedIds.contains(document.documentID) {
                        completedAchievements.append(achievement)
                    } else {
                        uncompletedAchievements.append(achievement)
                    }
                }
                
                completion([completedAchievements, uncompletedAchievements])
            }
    }
}
