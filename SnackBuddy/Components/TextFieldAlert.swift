//
//  TextFieldAlert.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/05/2022.
//

import SwiftUI

struct TextFieldAlert<Presenting>: View where Presenting: View {
    @Binding var isShowing: Bool
    @Binding var input: String
    let presenting: Presenting
    let title: String
    let inputTitle: String
    let buttons: [DialogButton]
    
    var body: some View {
        GeometryReader { (deviceSize: GeometryProxy) in
            ZStack {
                self.presenting
                    .overlay(isShowing ? Color.black.opacity(0.5) : Color.black.opacity(0))
                    .disabled(isShowing)
                
                VStack {
                    VStack {
                        Text(self.title)
                        TextField(self.inputTitle, text: self.$input)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }.padding()
                    
                    HStack(spacing: 0) {
                        ForEach(buttons) { button in
                            button
                        }
                    }
                }
                .background(.regularMaterial)
                .cornerRadius(10)
                .frame(
                    width: deviceSize.size.width * 0.7,
                    height: deviceSize.size.height * 0.7
                )
                .opacity(self.isShowing ? 1 : 0)
            }
        }
    }
}

struct TextFieldAlert_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color("Green")
                .edgesIgnoringSafeArea(.all)
                .overlay(Color.black.opacity(0.5).edgesIgnoringSafeArea(.all))
            
            VStack {}.textFieldAlert(
                isShowing: .constant(true),
                input: .constant(""),
                title: "Alert!",
                inputTitle: "Type...",
                buttons: [
                    DialogButton(text: "Button", action: {}),
                    DialogButton(text: "Button", action: {})
                ]
            )
        }
    }
}

extension View {
    func textFieldAlert(isShowing: Binding<Bool>, input: Binding<String>, title: String, inputTitle: String, buttons: [DialogButton]) -> some View {
        TextFieldAlert(
            isShowing: isShowing,
            input: input,
            presenting: self,
            title: title,
            inputTitle: inputTitle,
            buttons: buttons
        )
    }
}
