//
//  OnboardingView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 04/06/2022.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

struct OnboardingView: View {
    @FirestoreQuery(collectionPath: "onboarding") private var onboardingPages: [OnboardingPage]
    @EnvironmentObject private var errorHandler: ErrorHandler
    @ObservedObject private var viewModel = OnboardingViewModel()
    
    var body: some View {
        ZStack {
            viewModel.backgroundColor.edgesIgnoringSafeArea(.all)
            
            VStack {
                TabView(selection: $viewModel.currentPage) {
                    ForEach(onboardingPages.indices, id: \.self) { index in
                        VStack {
                            AsyncImage(url: URL(string: onboardingPages[index].image)) { image in
                                image
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                            } placeholder: {
                                ProgressView()
                            }.padding()
                            
                            HStack {
                                VStack(alignment: .leading) {
                                    Text(onboardingPages[index].title)
                                        .foregroundColor(
                                            Color(
                                                red: onboardingPages[index].foregroundColor,
                                                green: onboardingPages[index].foregroundColor,
                                                blue: onboardingPages[index].foregroundColor
                                            )
                                        )
                                        .font(.custom("JosefinSans-Bold", size: 48))
                                        .padding(.bottom)
                                
                                    Text(onboardingPages[index].content)
                                        .foregroundColor(
                                            Color(
                                                red: onboardingPages[index].foregroundColor,
                                                green: onboardingPages[index].foregroundColor,
                                                blue: onboardingPages[index].foregroundColor
                                            )
                                        )
                                        .font(.custom("Merriweather", size: 18))
                                }
                                .tag(index)
                                .padding()
                                
                                Spacer()
                            }
                        }
                    }
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                
                HStack {
                    HStack {
                        ForEach(onboardingPages.indices, id: \.self) { index in
                            Circle()
                                .foregroundColor(viewModel.currentPage == index ? viewModel.foregroundColor : viewModel.foregroundColor.opacity(0.3))
                                .frame(width: 12, height: 12)
                        }
                    }.padding()
                    
                    Spacer()
                    
                    SnackButton(
                        text: viewModel.nextButtonText(onboardingPagesCount: onboardingPages.count),
                        backgroundColor: .white,
                        foregroundColor: .black,
                        action: { viewModel.nextButtonAction(onboardingPagesCount: onboardingPages.count) },
                        icon: nil
                    )
                    .padding(.horizontal)
                    .disabled(onboardingPages.count == 0)
                }
                
                NavigationLink(destination: HomeView().withErrorHandling(), isActive: $viewModel.toHomeView) {
                    EmptyView()
                }
            }
        }
        .onChange(of: viewModel.currentPage) { index in
            viewModel.changeColor(index: index, onboardingPages: onboardingPages)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .textFieldAlert(
            isShowing: $viewModel.isShowingAlert,
            input: $viewModel.firstname,
            title: "Wat is je voornaam?",
            inputTitle: "Voornaam",
            buttons: [
                DialogButton(text: "OK", action: { viewModel.createUser(errorHandler: errorHandler) }),
                DialogButton(text: "Annuleren", action: { viewModel.toggleFirstNameAlert() })
            ]
        )
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
