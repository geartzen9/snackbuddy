//
//  Heading.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 04/06/2022.
//

import SwiftUI

struct Heading: View {
    let text: String
    let withoutPadding: Bool
    
    var body: some View {
        let mainView = Text(text).font(.custom("JosefinSans-Bold", size: 18))
        
        if withoutPadding {
            return AnyView(
                mainView
                    .padding(.top)
                    .padding(.bottom, 1)
            )
        }
        
        return AnyView(
            mainView
                .padding([.leading, .top, .trailing])
                .padding(.bottom, 1)
        )
    }
}

struct Heading_Previews: PreviewProvider {
    static var previews: some View {
        Heading(text: "Heading", withoutPadding: false)
    }
}
