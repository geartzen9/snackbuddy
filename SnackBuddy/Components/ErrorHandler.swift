//
//  ErrorHandler.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 22/04/2022.
//

import Combine
import SwiftUI
import os.log

/**
 A class that is used to convert the thrown error to an error message that can be shown as an `Alert` dialog.
 */
class ErrorHandler: ObservableObject {
    @Published var currentAlert: ErrorAlert?
    
    /**
     Sets the currentAlert to the thrown error so it can be displayed by an `Alert`.
     */
    func handle(error: Error) {
        currentAlert = ErrorAlert(message: error.localizedDescription)
    }
}

extension Logger {
    private static var subsystem = Bundle.main.bundleIdentifier!

    /// Logs the messages of the documents from the database store.
    static let documents = Logger(subsystem: subsystem, category: "documents")
    
    /// Logs the messages about the current user .
    static let user = Logger(subsystem: subsystem, category: "user")
}
