//
//  NameFormView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 07/04/2022.
//

import SwiftUI

struct NameFormSheet: View {
    @ObservedObject var viewModel = NameFormSheetModel()
//    @State private var firstname: String = ""
    @EnvironmentObject var errorHandler: ErrorHandler
//    @State var isShown: Bool = false
    
//    init() {
//        self.viewModel = NameFormSheetModel(isShown: self.isShown)
//    }
    
    var body: some View {
        NavigationView {
            CircleLayout(
                topChild: {
                    HStack {
                        VStack(alignment: .leading) {
                            Spacer()
                            
                            Text("Wat is jouw voornaam?")
                                .foregroundColor(.black)
                                .font(.custom("JosefinSans-Bold", size: 38))
                                .padding(.bottom)
    
                            TextField("Voornaam", text: $viewModel.firstname)
                                .padding()
                                .background(Color(UIColor.systemBackground))
                                .cornerRadius(20)
                                .foregroundColor(Color(UIColor.systemGray))
                                .font(.custom("Merriweather", size: 18))
    
                            Spacer()
                        }
                        .padding()
                        
                        Spacer()
                    }
                },
                withBackButton: false
            )
            .interactiveDismissDisabled()
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: Button(action: { viewModel.createUser(errorHandler: errorHandler) }) {
                Text("Gereed").font(.custom("Merriweather", size: 18))
            }.disabled(viewModel.firstname == ""))
        }
    }
}

struct NameFormView_Previews: PreviewProvider {
    static var previews: some View {
        NameFormSheet(
//            showing: true
        )
    }
}
