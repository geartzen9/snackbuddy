//
//  SnackBuddyApp.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 06/03/2022.
//

import SwiftUI
import Firebase

@main
struct SnackBuddyApp: App {
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        return WindowGroup {
            NavigationView {
                if UserDefaults.standard.string(forKey: "firstname") != nil {
                    AnyView(HomeView().withErrorHandling())
                } else {
                    AnyView(OnboardingView().withErrorHandling())
                }
            }
        }
    }
}
