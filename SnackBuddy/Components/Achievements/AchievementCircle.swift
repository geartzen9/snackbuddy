//
//  AchievementCircle.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/03/2022.
//

import SwiftUI

struct AchievementCircle: View {
    let icon: String
    let color: Color
    let big: Bool
    
    var body: some View {
        ZStack {
            Circle()
                .foregroundColor(color)
                .scaledToFit()
            
            Text(icon)
                .font(.system(size: big ? 120 : 42))
        }
    }
}

struct AchievementCircle_Previews: PreviewProvider {
    static var previews: some View {
        AchievementCircle(
            icon: "❤️",
            color: Color("Red"),
            big: true
        )
    }
}
