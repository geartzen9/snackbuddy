//
//  SnackRepository.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/05/2022.
//

import FirebaseFirestore
import os.log

class SnackRepository: Repository {
    typealias T = Snack
    
    var path = "snacks"
    var store = Firestore.firestore()
    
    /**
     Sets the settings of the firestore instance.
     */
    init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        
        store.settings = settings
    }
    
    /**
     Gets all `Snack`s from the database store.
     */
    func getAll(completion: @escaping ([Snack]?) -> Void) {
        store.collection(path)
            .getDocuments { snapshot, error in
                if let error = error {
                    Logger.documents.error("Error retrieving snack: \(error.localizedDescription)")
                    completion(nil)
                    return
                }
                
                var snacks: [Snack] = []
                
                for document in snapshot!.documents {
                    guard let snack = try? document.data(as: Snack.self) else { return }
                    snacks.append(snack)
                }
                
                completion(snacks)
            }
    }
}
