//
//  AchievementProgressBar.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 18/06/2022.
//

import SwiftUI

struct AchievementProgressBar: View {
    let currentProgress: Int
    let neededProgress: Int

    var body: some View {
        VStack {
            ZStack {
                // Background
                Capsule()
                    .fill(Color("Yellow"))
                    .frame(width: .infinity, height: 50)
            
                GeometryReader { geometryReader in
                    // Actual progress
                    HStack() {
                        Capsule()
                            .fill(.white.opacity(0.5))
                            .frame(width: geometryReader.size.width / 2, height: 50)
                    }.frame(maxWidth: .infinity, alignment: .leading)
                }
            }
            
            HStack {
                Text("\(currentProgress) / \(neededProgress)")
                    .font(.custom("Merriweather", size: 16))
                
                Spacer()
            }
        }
    }
}

struct AchievementProgressBar_Previews: PreviewProvider {
    static var previews: some View {
        AchievementProgressBar(
            currentProgress: 1,
            neededProgress: 2
        )
    }
}
