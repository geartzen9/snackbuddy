//
//  BuddyShopItemView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 20/06/2022.
//

import SwiftUI

struct BuddyShopItemView: View {
    let buddyShopItem: BuddyShopItem
    
    var body: some View {
        CircleLayout(
            alternative: false,
            topChild: {
                AppBar(withBackButton: true, title: "BuddyShop")
                
                VStack(alignment: .leading) {
                    HStack {
                        AsyncImage(url: URL(string: buddyShopItem.image)) { image in
                            image
                                .resizable()
                                .scaledToFill()
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .frame(height: 200)
                        } placeholder: {
                            ProgressView()
                        }.cornerRadius(15)
                        
                        Spacer()
                    }
                    
                    Text(buddyShopItem.title)
                        .font(.custom("Merriweather", size: 18))
                        .padding(.top)
                }.padding()
                
                Spacer()
            },
            containerChild: {
                VStack(alignment: .leading) {
                    Heading(text: "Kosten", withoutPadding: true)
                    CoinsContainer(amount: buddyShopItem.cost, backgroundColor: Color("Yellow"), foregroundColor: Color("Green"))
                    
                    Heading(text: "Omschrijving", withoutPadding: true)
                    Text(buddyShopItem.description).font(.custom("Merriweather", size: 18))
                    
                    SnackButton(
                        text: "Aanschaffen",
                        backgroundColor: Color("Green"),
                        foregroundColor: .white,
                        action: {
                            
                        },
                        icon: "checkmark.circle.fill"
                    )
                }
                .padding()
                .frame(maxWidth: .infinity, alignment: .leading)
            }
        )
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

struct BuddyShopItemView_Previews: PreviewProvider {
    static var previews: some View {
        BuddyShopItemView(
            buddyShopItem: BuddyShopItem(
                image: "https://via.placeholder.com/300.png/",
                title: "Swapfiets",
                description: "Dit is een omschrijving.",
                cost: 200
            )
        )
    }
}
