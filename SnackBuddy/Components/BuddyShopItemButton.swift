//
//  BuddyShopItem.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 20/06/2022.
//

import SwiftUI

struct BuddyShopItemButton: View {
    let buddyShopItem: BuddyShopItem
    
    var body: some View {
        NavigationLink(destination: BuddyShopItemView(buddyShopItem: buddyShopItem)) {
            VStack {
                ZStack(alignment: .topTrailing) {
                    AsyncImage(url: URL(string: buddyShopItem.image)) { image in
                        image
                            .resizable()
                            .scaledToFill()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .frame(height: 200)
                    } placeholder: {
                        ProgressView()
                    }.cornerRadius(15)
                    
                    Text("\(buddyShopItem.cost) coins")
                        .font(.custom("Merriweather", size: 18))
                        .padding(5)
                        .foregroundColor(.black)
                        .background(Color("Yellow"))
                        .cornerRadius(15)
                        .padding(5)
                }
                
                Text(buddyShopItem.title)
                    .font(.custom("Merriweather", size: 18))
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            }
        }
    }
}

struct BuddyShopItemButton_Previews: PreviewProvider {
    static var previews: some View {
        BuddyShopItemButton(
            buddyShopItem: BuddyShopItem(
                image: "https://via.placeholder.com/300.png/",
                title: "Title",
                description: "Dit is een omschrijving.",
                cost: 200
            )
        )
    }
}
