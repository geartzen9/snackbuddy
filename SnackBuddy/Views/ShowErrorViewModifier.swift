//
//  ShowErrorViewModifier.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 22/04/2022.
//

import SwiftUI

struct ShowErrorViewModified: ViewModifier {
    @StateObject var errorHandler = ErrorHandler()
    
    func body(content: Content) -> some View {
        content
            .environmentObject(errorHandler)
            .background(
                EmptyView()
                    .alert(item: $errorHandler.currentAlert) { currentAlert in
                        Alert(
                            title: Text("Er is een fout opgetreden"),
                            message: Text(currentAlert.message),
                            dismissButton: .default(Text("OK")) {
                                currentAlert.dismissAction?()
                            }
                        )
                    }
            )
    }
}

extension View {
    func withErrorHandling() -> some View {
        modifier(ShowErrorViewModified())
    }
}
