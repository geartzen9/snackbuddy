//
//  OnboardingPage.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/03/2022.
//

import Foundation
import FirebaseFirestoreSwift

struct OnboardingPage: Identifiable, Decodable {
    @DocumentID var id: String?
    let title: String
    let content: String
    let image: String
    let backgroundColor: String
    let foregroundColor: Double
}
