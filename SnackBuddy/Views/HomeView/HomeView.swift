//
//  HomeView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/03/2022.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject private var errorHandler: ErrorHandler
    @ObservedObject private var viewModel = HomeViewModel()
    @AppStorage("firstname") private var firstname: String = "Onbekend"
    
    var body: some View {
        CircleLayout(
            alternative: false,
            topChild: {
                VStack {
                    AppBar(
                        withBackButton: false,
                        title: "\(viewModel.getGreeterText())\n\(firstname)",
                        secondaryChild: {
                            NavigationLink(destination: BuddyShopView().withErrorHandling) {
                                Image(systemName: "cart.fill")
                                    .foregroundColor(.black)
                                    .font(.system(size: 28.0))
                            }
                        }
                    )
                    
                    AnyView(
                        viewModel.randomSnack != nil ? AnyView(
                            VStack {
                                viewModel.buildSnackImageView()
                                
                                HStack {
                                    VStack {
                                        Text(viewModel.choosenSnack != nil ? "Je hebt voor \(viewModel.getDayPart()) gekozen voor:" : "Probeer \(viewModel.getDayPart()) eens:")
                                            .font(.custom("Merriweather", size: 16))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                        
                                        Text(viewModel.choosenSnack != nil ? viewModel.choosenSnack!.name : viewModel.randomSnack!.name)
                                            .font(.custom("Merriweather", size: 16).weight(.bold))
                                            .frame(maxWidth: .infinity, alignment: .leading)
                                    }
                                    
                                    Spacer()
                                }.padding(.horizontal)
                                
                                HStack {
                                    SnackButton(
                                        text: "Oké",
                                        backgroundColor: Color("Green"),
                                        foregroundColor: .white,
                                        action: { viewModel.chooseSnack() },
                                        icon: "checkmark.circle.fill"
                                    )
                                    .disabled(viewModel.choosenSnack != nil)
                                    .opacity(viewModel.choosenSnack != nil ? 0.5 : 1)
                                    
                                    SnackButton(
                                        text: "Iets anders",
                                        backgroundColor: Color("Red"),
                                        foregroundColor: .white,
                                        action: { viewModel.retrieveRandomSnack(errorHandler: errorHandler) },
                                        icon: "x.circle.fill"
                                    )
                                    .disabled(viewModel.choosenSnack != nil)
                                    .opacity(viewModel.choosenSnack != nil ? 0.5 : 1)
                                }.padding(.horizontal)
                            }
                        ) : AnyView(
                            Text("Je hebt al een snack uitgekozen voor dit moment.")
                        )
                    ).onAppear {
                        viewModel.retrieveRandomSnack(errorHandler: errorHandler)
                    }
                    
                    Spacer()
                }
            },
            containerChild: {
                VStack(alignment: .leading) {
                    Heading(text: "Volgende prestaties", withoutPadding: false)

                    ScrollView(.horizontal) {
                        viewModel.buildAchievementRow(achievements: viewModel.uncompletedAchievements)
                    }
                    
                    Heading(text: "Behaalde prestaties", withoutPadding: false)

                    ScrollView(.horizontal) {
                        viewModel.buildAchievementRow(achievements: viewModel.completedAchievements)
                    }
                }
                .frame(maxWidth: .infinity, alignment: .leading)
                .onAppear {
                    viewModel.retrieveAchievements(errorHandler: errorHandler)
                }
            }
        )
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().withErrorHandling()
    }
}
