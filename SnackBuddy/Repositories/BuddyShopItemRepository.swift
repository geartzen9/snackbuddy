//
//  BuddyShopItemRepository.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 20/06/2022.
//

import FirebaseFirestore
import os.log

class BuddyShopItemRepository: Repository {
    typealias T = BuddyShopItem
    
    var path = "shop-items"
    var store = Firestore.firestore()
    
    /**
     Sets the settings of the firestore instance.
     */
    init() {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        
        store.settings = settings
    }
    
    /**
     Gets all `BuddyShopItem`s from database store.
     */
    func getAll(completion: @escaping ([BuddyShopItem]?) -> Void) {
        store.collection(path)
            .getDocuments { snapshot, error in
                if let error = error {
                    Logger.documents.error("Error retrieving BuddyShop item: \(error.localizedDescription)")
                    completion(nil)
                    return
                }
                
                var buddyShopItems: [BuddyShopItem] = []
                
                for document in snapshot!.documents {
                    guard let buddyShopItem = try? document.data(as: BuddyShopItem.self) else { return }
                    buddyShopItems.append(buddyShopItem)
                }
                
                completion(buddyShopItems)
            }
    }
}
