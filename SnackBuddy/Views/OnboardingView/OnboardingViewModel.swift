//
//  OnboardingViewModel.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/05/2022.
//

import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift

class OnboardingViewModel: ObservableObject {
    private var userRepository = UserRepository()
    
    @Published var currentPage: Int = 0
    @Published var backgroundColor: Color = Color("Red")
    @Published var foregroundColor: Color = .white
    @Published var toHomeView: Bool = false
    @Published var isShowingAlert = false
    @Published var firstname = ""
    
    /**
     Changes the background and foreground color of the onboarding screens to the correct color.
     
     - Parameters:
        - index: The index of the onboarding page that is being navigated to.
        - onboardingPages: The array of onboarding pages that has been retreived from the database store.
     */
    func changeColor(index: Int, onboardingPages: [OnboardingPage]) {
        let foreground: Double = onboardingPages[index].foregroundColor
        
        withAnimation(.easeInOut(duration: 0.3)) {
            backgroundColor = Color(onboardingPages[index].backgroundColor)
            foregroundColor = Color(red: foreground, green: foreground, blue: foreground)
        }
    }
    
    /**
     Determines the text that needs to be displayed on the "next button" of the onboarding.
     
     - Parameter onboardingPagesCount: The amount of onboarding pages.
     
     - Returns: A string that can be used as text for the "next button" of the onboarding.
     */
    func nextButtonText(onboardingPagesCount: Int) -> String {
        if currentPage != onboardingPagesCount - 1 {
            return "Volgende"
        }
        
        return "Beginnen"
    }
    
    /**
     Checks if the user is on the last page of the onboarding and shows the firstname dialog.
     When the user is not on the last page, it cycles to the next page.
     
     - Parameter onboardingPagesCount: The amount of onboarding pages.
     */
    func nextButtonAction(onboardingPagesCount: Int) {
        if currentPage != onboardingPagesCount - 1 {
            withAnimation { currentPage += 1 }
        } else {
            toggleFirstNameAlert()
        }
    }
    
    /**
     Shows or hides the alert for entering the firstname of the user.
     */
    func toggleFirstNameAlert() {
        withAnimation {
            isShowingAlert.toggle()
        }
    }
    
    /**
     Creates a new user in database store using the `userRepository`.
     
     - Parameter errorHandler: The `ErrorHandler` object that is used to show an error message when something goes wrong.
     
     - Throws: `DatabaseError.addingError`
     If an error occurs when creating a new user in the database store.
     */
    func createUser(errorHandler: ErrorHandler) {
        if firstname == "" {
            return
        }
        
        userRepository.add { userId in
            if userId == nil {
                errorHandler.handle(error: DatabaseError.addingError)
            } else {
                let defaults = UserDefaults.standard
                
                defaults.set(self.firstname, forKey: "firstname")
                defaults.set(userId, forKey: "user_id")
                self.toHomeView.toggle()
            }
        }
    }
}
