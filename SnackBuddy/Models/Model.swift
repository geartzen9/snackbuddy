//
//  Model.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 01/04/2022.
//

import Foundation
import FirebaseFirestoreSwift

protocol Model: Identifiable, Codable {
    var id: String? {get}
    
    /**
     Converts the current model instance to a Dictionary type.
     
     - Returns: The Dictionary that represents the current model instance.
     */
//    func asDictionary() -> [String: Any]
    
    /**
     Converts the given Dictionary object to a new model instance.
     
     - Parameter dictionary: The Dictionary that needs to be converted to a model instance.
     
     - Returns: A new instance of a model converted from the given Dictionary
     */
//    static func fromDictionary(_ dictionary: [String: Any]) -> Self?
}
