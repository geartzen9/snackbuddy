//
//  CircleLayout.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 07/04/2022.
//

import SwiftUI

struct CircleLayout<ContainerContent: View, Content : View>: View {
    let alternative: Bool
    let topChild: Content
    let containerChild: ContainerContent
    
    /**
     The full constructor.
     
     - Parameters:
        - topChild: The child that is displayed on the top of the screen.
        - containerChild: The child that is displayed in the bottom container.
        - withBigCircle: Set to true when a big circle needs to be displayed. When set to false, a smaller circle is displayed.
     */
    init(
        alternative: Bool,
        @ViewBuilder topChild: () -> Content,
        @ViewBuilder containerChild: () -> ContainerContent
    ) {
        self.alternative = alternative
        self.topChild = topChild()
        self.containerChild = containerChild()
    }
    
    var body: some View {
        let mainView = AnyView(
            ZStack {
                Color("Yellow").edgesIgnoringSafeArea(ContainerContent.self != EmptyView.self ? .top : .all)
                
                GeometryReader { geometry in
                    Ellipse()
                        .foregroundColor(.white)
                        .opacity(0.5)
                        .frame(width: geometry.size.width, height: geometry.size.height)
                        .position(x: geometry.size.width / 4, y: 100)
                        .clipped()
                }.edgesIgnoringSafeArea(.top)
                
                VStack {
                    topChild
                    
                    if ContainerContent.self != EmptyView.self {
                        ZStack {
                            Color(UIColor.systemBackground)
                                .cornerRadius(20, corners: [.topLeft, .topRight])
                            
                            HStack {
                                ScrollView { containerChild }
                            }
                        }
                    }
                }
            }
        )
        
        let alternativeView = AnyView(
            VStack {
                ZStack {
                    GeometryReader { geometry in
                        Ellipse()
                            .fill(Color("Yellow"))
                            .frame(width: geometry.size.width * 2, height: geometry.size.height * 2)
                            .position(x: 25, y: 0)
                            .clipped()
                    }.edgesIgnoringSafeArea(.top)
                    
                    topChild
                }.fixedSize(horizontal: false, vertical: true)
                
                VStack {
                    if ContainerContent.self != EmptyView.self {
                        ScrollView {
                            containerChild
                        }
                    }
                }
            }
        )
        
        if alternative {
            return alternativeView
        }
        
        return mainView;
    }
}

extension CircleLayout where ContainerContent == EmptyView {
    /**
     A constructor without the `containerContent` parameter to make it optional.
     
     - Parameters:
        - topChild: The child that is displayed on the top of the screen.
     */
    init(
        alternative: Bool,
        @ViewBuilder topChild: () -> Content
    ) {
        self.init(
            alternative: alternative,
            topChild: topChild,
            containerChild: { EmptyView() }
        )
    }
}

struct CircleLayout_Previews: PreviewProvider {
    static var previews: some View {
        CircleLayout(
            alternative: true,
            topChild: { EmptyView() }
        )
    }
}
