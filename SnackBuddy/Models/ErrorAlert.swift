//
//  ErrorAlert.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 22/04/2022.
//

import Foundation

/**
 A model that holds the information that a error alert needs to display.
 */
struct ErrorAlert: Identifiable {
    var id = UUID()
    var message: String
    var dismissAction: (() -> Void)?
}
