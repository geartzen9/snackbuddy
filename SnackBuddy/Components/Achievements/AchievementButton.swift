//
//  AchievementButton.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 04/06/2022.
//

import SwiftUI

struct AchievementButton: View {
    let achievement: Achievement
    let color: Color
    
    var body: some View {
        NavigationLink(destination: AchievementView(achievement: achievement, color: color)) {
            VStack(alignment: .center) {
                AchievementCircle(
                    icon: achievement.icon,
                    color: color,
                    big: false
                )
                
                Text(achievement.name)
                    .font(.custom("Merriweather", size: 14))
                    .foregroundColor(.black)
                    .multilineTextAlignment(.center)
            }.frame(width: 80)
        }
    }
}

struct AchievementButton_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            AchievementButton(
                achievement: Achievement(
                    icon: "❤️",
                    name: "2 dagen overleefd",
                    reward: 200
                ),
                color: Color("Red")
            )
        }
    }
}
