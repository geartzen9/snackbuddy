//
//  DialogButton.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/05/2022.
//

import SwiftUI

struct DialogButton: View, Identifiable {
    let id = UUID()
    let text: String
    let action: () -> Void
    
    var body: some View {
        Button(action: action) {
            Text(text)
                .padding()
                .frame(maxWidth: .infinity)
                .background(.white.opacity(0))
                .foregroundColor(.blue)
                .border(.gray.opacity(0.2))
        }
    }
}

struct DialogButton_Previews: PreviewProvider {
    static var previews: some View {
        DialogButton(
            text: "Text",
            action: {}
        )
    }
}
