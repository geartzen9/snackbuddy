//
//  CoinsContainerViewModel.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 28/06/2022.
//

import SwiftUI

class CoinsContainerViewModel {
    /**
     Builds the `Text` view that displays the amount of coins.
     
     - Parameters:
        - amount: The number of coins that needs to be displayed in the container.
        - foregroundColor: The foreground color of the container.
        - backgroundColor: The background color of the container. Centers the text inside the container when not `nil`.
     
     - Returns: A `Text` view with the amount of coins, or when the given amount is `nil`, a `Text` view with "Onbekend" as content.
     */
    func buildTextView(amount: Int?, foregroundColor: Color, backgroundColor: Color? = nil) -> some View {
        let content = amount != nil ? "\(amount!) Coins" : "Onbekend"
        let textView = Text(content)
            .font(.custom("JosefinSans-Bold", size: 38))
            .foregroundColor(foregroundColor)
        
        if backgroundColor != nil {
            return AnyView(
                textView
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(backgroundColor)
                    .cornerRadius(15)
            )
        }
        
        return AnyView(
            textView.frame(maxWidth: .infinity, alignment: .leading)
        )
    }
}
