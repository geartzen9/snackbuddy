//
//  ShopView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/03/2022.
//

import SwiftUI

struct BuddyShopView: View {
    @EnvironmentObject private var errorHandler: ErrorHandler
    @ObservedObject private var viewModel = BuddyShopViewModel()
    
    var body: some View {
        CircleLayout(
            alternative: true,
            topChild: {
                VStack {
                    AppBar(withBackButton: true, title: "BuddyShop")
                    
                    CoinsContainer(
                        amount: viewModel.coinsCount,
                        backgroundColor: Color("Green"),
                        foregroundColor: Color("Yellow"),
                        headingText: "Huidig saldo:"
                    ).padding([.bottom, .leading, .trailing])
                }
            },
            containerChild: {
                viewModel.buddyShopItems != nil ? AnyView(
                    LazyVGrid(columns: [GridItem(.flexible())], spacing: 25) {
                        ForEach(viewModel.buddyShopItems!) { buddyShopItem in
                            BuddyShopItemButton(buddyShopItem: buddyShopItem)
                        }
                    }.padding([.bottom, .leading, .trailing])
                ) : AnyView(
                    ProgressView()
                )
            }
        ).onAppear {
            viewModel.retrieveAllItems(errorHandler: errorHandler)
            viewModel.retrieveCoinsCount(errorHandler: errorHandler)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

struct BuddyShopView_Previews: PreviewProvider {
    static var previews: some View {
        BuddyShopView().withErrorHandling()
    }
}
