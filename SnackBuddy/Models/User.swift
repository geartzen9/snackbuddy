//
//  User.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 30/03/2022.
//

import Foundation
import FirebaseFirestoreSwift

final class User: Model {
    @DocumentID var id: String?
    var completedAchievements: [String]
    let coins: Int
    
    init(completedAchievements: [String], coins: Int) {
        self.completedAchievements = completedAchievements
        self.coins = coins
    }
}
