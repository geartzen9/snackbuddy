//
//  BuddyShopViewModel.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 20/06/2022.
//

import SwiftUI
import os.log

class BuddyShopViewModel: ObservableObject {
    private var buddyShopItemRepository = BuddyShopItemRepository()
    private var userRepository = UserRepository()
    
    @Published var buddyShopItems: [BuddyShopItem]?
    @Published var coinsCount: Int?
    
    /**
     Retrieves all BuddyShop items from the `BuddyShopItemRepository`.
     
     - Parameter errorHandler: The `ErrorHandler` object that is used to show an error message when something goes wrong.
     
     - Throws: `DatabaseError.retrievingError`
     If an error occurs when retrieving the BuddyShop items from the database store.
     */
    func retrieveAllItems(errorHandler: ErrorHandler) {
        buddyShopItemRepository.getAll { buddyShopItems in
            if buddyShopItems != nil {
                self.buddyShopItems = buddyShopItems
            } else {
                errorHandler.handle(error: DatabaseError.retrievingError)
            }
        }
    }
    
    /**
     Retrieve the total of coins the user has earned.
     
     - Parameter errorHandler: The `ErrorHandler` object that is used to show an error message when something goes wrong.
     */
    func retrieveCoinsCount(errorHandler: ErrorHandler) {
        let userId = UserDefaults.standard.string(forKey: "user_id")
        
        if userId == nil {
            coinsCount = -1
            Logger.user.error("Error retrieving the user ID, it is nil")
            errorHandler.handle(error: UserError.retrievingError)
            return
        }
        
        userRepository.getById(userId!) { user in
            self.coinsCount = user.coins
        }
    }
}
