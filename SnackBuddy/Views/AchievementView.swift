//
//  AchievementView.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 04/06/2022.
//

import SwiftUI

struct AchievementView: View {
    let achievement: Achievement
    let color: Color
    
    var body: some View {
        CircleLayout(
            alternative: false,
            topChild: {
                VStack {
                    AppBar(withBackButton: true, title: "Prestatie")
                    
                    VStack(alignment: .leading) {
                        HStack {
                            AchievementCircle(
                                icon: achievement.icon,
                                color: color,
                                big: true
                            )
                            
                            Spacer()
                        }
                        
                        Text(achievement.name)
                            .font(.custom("Merriweather", size: 18))
                            .padding(.top)
                    }.padding()
                    
                    Spacer()
                }
            },
            containerChild: {
                VStack(alignment: .leading) {
                    Heading(text: "Voortgang", withoutPadding: true)
                    AchievementProgressBar(
                        currentProgress: 1,
                        neededProgress: 2
                    )                    
                    Heading(text: "Beloning", withoutPadding: true)
                    CoinsContainer(
                        amount: achievement.reward,
                        backgroundColor: Color("Red"),
                        foregroundColor: .white
                    )
                    
                    Heading(text: "Jouw saldo na behalen", withoutPadding: true)
                    CoinsContainer(
                        amount: achievement.reward,
                        backgroundColor: Color("Green"),
                        foregroundColor: Color("Yellow")
                    )
                }
                .padding()
                .frame(maxWidth: .infinity, alignment: .leading)
            }
        )
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

struct AchievementView_Previews: PreviewProvider {
    static var previews: some View {
        AchievementView(
            achievement: Achievement(
                icon: "❤️",
                name: "2 dagen overleefd",
                reward: 200
            ),
            color: Color("Red")
        )
    }
}
