//
//  AppBar.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 08/04/2022.
//

import SwiftUI

struct AppBar<SecondaryChild: View>: View {
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var withBackButton: Bool
    var title: String
    let secondaryChild: SecondaryChild
    
    init(withBackButton: Bool, title: String, @ViewBuilder secondaryChild: () -> SecondaryChild) {
        self.withBackButton = withBackButton
        self.title = title
        self.secondaryChild = secondaryChild()
    }
    
    var body: some View {
        HStack {
            if withBackButton {
                Button(
                    action: { self.mode.wrappedValue.dismiss() },
                    label: {
                        Label {
                            Text(title)
                                .font(.custom("JosefinSans-Bold", size: 28))
                                .scaledToFit()
                                .minimumScaleFactor(0.5)
                                .foregroundColor(.black)
                        } icon: {
                            Image(systemName: "chevron.left")
                                .foregroundColor(.black)
                                .font(.system(size: 22.0))
                        }
                    }
                )
            } else {
                Text(title)
                    .font(.custom("JosefinSans-Bold", size: 28))
                    .scaledToFit()
                    .minimumScaleFactor(0.5)
                    .foregroundColor(.black)
            }

            Spacer()

            secondaryChild
        }.padding(.horizontal)
    }
}

extension AppBar where SecondaryChild == EmptyView {
    init(withBackButton: Bool, title: String) {
        self.init(
            withBackButton: withBackButton,
            title: title,
            secondaryChild: { EmptyView() }
        )
    }
}

struct AppBar_Previews: PreviewProvider {
    static var previews: some View {
        AppBar(
            withBackButton: true,
            title: "Title",
            secondaryChild: {
                NavigationLink(destination: BuddyShopView().withErrorHandling) {
                    Image(systemName: "cart.fill")
                        .foregroundColor(.black)
                        .font(.system(size: 28.0))
                }
            }
        )
    }
}
