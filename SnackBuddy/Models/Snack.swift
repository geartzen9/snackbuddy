//
//  Snack.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 29/05/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

struct Snack: Model {
    @DocumentID var id: String?
    let image: String
    let name: String
}
