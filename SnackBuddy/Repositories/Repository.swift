//
//  Repository.swift
//  SnackBuddy
//
//  Created by Gerrit de Heij on 01/04/2022.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift

protocol Repository: ObservableObject {
    associatedtype T
    
    /**
     The path of the collection in the database store.
     */
    var path: String {get}
    
    /**
     The Firestore instance that is used to interact with the database store.
     */
    var store: Firestore {get}
}
